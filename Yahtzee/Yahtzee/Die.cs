﻿using System;

namespace Yahtzee
{
    public class Die
    {
        //Attributes
        public int NumSides;
        public int CurrentValue;
        public string DieName = "D";
        public string Range;

        //Constructor
        public Die(int numSides)
        {
            this.NumSides = numSides;
            this.DieName = DieName + numSides.ToString();
            this.Range = ("1 - " + numSides);
            this.roll();
        }

        public Die()
        {
            this.NumSides = 6;
            this.DieName += this.NumSides;
            this.Range = ("1 - " + this.NumSides);
            this.roll();
        }

        //Getters and Setters
        public int getNumSides()
        {
            return this.NumSides;
        }

        public void setNumSides(int numSides)
        {
            if (numSides > 1 && numSides <= this.NumSides)
            {
                this.NumSides = numSides;
            }
            else
            {
                Console.WriteLine("The entry {0} must be greater than 0", numSides);
            }
        }

        public int getCurrentValue()
        {
            return this.CurrentValue;
        }

        //Methods
        public string toString()
        {
            return this.CurrentValue.ToString();
        }

        public void printSummary()
        {
            Console.WriteLine("Test");
            Console.WriteLine("------");
            Console.WriteLine("The die name is: {0}", this.DieName);
            Console.WriteLine("The number of sides is: {0}", this.NumSides);
            Console.WriteLine("The current value is: {0}", this.CurrentValue);
            Console.WriteLine("The range of {0} is {1}", this.DieName, this.Range);
        }

        public void roll()
        {
            var random = new Random();
            var randomInt = random.Next(1, this.NumSides + 1);
            this.CurrentValue = randomInt;
        }

        public void setCurrentValue(int cheat)
        {
            if (cheat > 0 && cheat <= this.NumSides)
            {
                this.CurrentValue = cheat;
            }
            else if (cheat <= 0)
            {
                Console.WriteLine("The cheat value must be greater than zero! {0}", cheat);
            }
            else
            {
                Console.WriteLine("The cheat value {0} provided is not valid must be within range: {1}", cheat, this.Range);
            }
        }

        public void setCurrentValueOverride(int reallyCheat)
        {
            var random = new Random();
            var randomInt = random.Next(int.MinValue, int.MaxValue);
            this.CurrentValue = randomInt;
        }

    }
}
