﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yahtzee
{
    class YahtzeeHandTester
    {
        static void Main(string[] args)
        {
            var newHand = new YahtzeeHand();
            Console.WriteLine("Default Hand Count = {0} Default Hand = {1} Current count of 2s = {2} Current faceValue of 5 = {3}", 
                newHand.Dice.Count, 
                newHand.ToString(),
                newHand.CountDice(2),
                newHand.FaceValue(5));
            newHand.RollDice();
            Console.WriteLine("Default Hand Count = {0} and First Roll Hand = {1}", newHand.Dice.Count, newHand.ToString());
            newHand.RollDice();
            Console.WriteLine("Default Hand Count = {0} and Second Roll Hand = {1}", newHand.Dice.Count, newHand.ToString());

            var newHandOne = new YahtzeeHand(11);
            Console.WriteLine("Default Hand Count = {0} and Default Hand = {1}", newHandOne.Dice.Count, newHandOne.ToString());
            var testList = new List<int>
            {
                1,1,1,4,5,6,7,8,9,10,11
            };
            newHandOne.setDice(testList);
            Console.WriteLine("Default Hand Count = {0} and Default Hand = {1} Is Three Kind Value = {2}", newHandOne.Dice.Count, newHandOne.ToString(), newHandOne.ThreeKindValue());
            newHandOne.RollDice();
            Console.WriteLine("Default Hand Count = {0} and First Roll Hand = {1}", newHandOne.Dice.Count, newHandOne.ToString());
            newHandOne.RollDice();
            Console.WriteLine("Default Hand Count = {0} and Second Roll Hand = {1}", newHandOne.Dice.Count, newHandOne.ToString());

            var newHandTwo = new YahtzeeHand(7, 12);
            Console.WriteLine("Default Hand Count = {0} and Default Hand = {1}", newHandTwo.Dice.Count, newHandTwo.ToString());
            var testListTwo = new List<int>
            {
                1,6,7,10,10,10,10
            };
            newHandTwo.setDice(testListTwo);
            Console.WriteLine("Default Hand Count = {0} and Default Hand = {1} is Four Kind Value = {2}", newHandTwo.Dice.Count, newHandTwo.ToString(), newHandTwo.FourKindValue());
            newHandTwo.RollDice();
            Console.WriteLine("Default Hand Count = {0} and First Roll Hand = {1}", newHandTwo.Dice.Count, newHandTwo.ToString());
            newHandTwo.RollDice();
            Console.WriteLine("Default Hand Count = {0} and Second Roll Hand = {1}", newHandTwo.Dice.Count, newHandTwo.ToString());

            var newHandThree = new YahtzeeHand();
            Console.WriteLine("Default Hand Count = {0} and Default Hand = {1}", newHandThree.Dice.Count, newHandThree.ToString());
            var testListThree = new List<int>
            {
                5,5,5,6,6,
            };
            newHandThree.setDice(testListThree);
            Console.WriteLine("Default Hand Count = {0} and Default Hand = {1} is Full House Value = {2}", newHandThree.Dice.Count, newHandThree.ToString(), newHandThree.FullHouseValue());

        }
    }
}
