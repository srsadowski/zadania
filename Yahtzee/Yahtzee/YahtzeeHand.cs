﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yahtzee
{
    public class YahtzeeHand
    {
        public const int ThreeKind = 3;
        public const int FourKind = 4;

        //Attributes
        public List<Die> Dice;
        public Die[] DiceArray;

        //Constructors
        public YahtzeeHand()
        {
            SetupDice();
        }

        public YahtzeeHand(int numDice)
        {
            this.SetupDice(numDice);
        }

        public YahtzeeHand(int numDice, int numSides)
        {
            this.SetupDice(numDice, numSides);
        }


        private void SetupDice(int numDice = 5, int numSides = 6)
        {
            this.Dice = new List<Die>();
            for (int i = 0; i < numDice; i++)
            {
                var newDie = new Die(numSides);
                this.Dice.Add(newDie);
                this.DiceArray = Dice.ToArray();
            }
        }

        //instance methods
        public void RollDice()
        {
            foreach (var die in this.Dice)
            {
                die.roll();
            }
        }

        public string ToString()
        {
            var output = string.Empty;
            foreach (var die in this.Dice)
            {
                var dieString = die.toString();
                output += dieString + " ";
            }

            return output;
        }

        public int CountDice(int givenValue)
        {
            int count = 0;
            return count = this.Dice.Count(x => x.CurrentValue == givenValue);
        }

        public void setDice(List<int> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                this.Dice[i].CurrentValue = values[i];
            }
        }

        //Scoring Method
        public int FaceValue(int n)
        {
            int result;
            return result = this.CountDice(n) * n;
        }

        public int ThreeKindValue()
        {
            return CheckKindValue(ThreeKind);
        }

        public int FourKindValue()
        {
            return CheckKindValue(FourKind);
        }

        public int FullHouseValue()
        {
            var threeCount = false;
            var twoCount = false;
            var result = 0;

            var groups = this.Dice.GroupBy(x => x.CurrentValue)
                                  .Select(y => new
                                  {
                                      Value = y.Key,
                                      Count = y.Count()
                                  })
                                  .OrderBy(x => x.Value);

            foreach (var group in groups)
            {
                if (group.Count == 3)
                {
                    threeCount = true;
                }

                if (group.Count == 2)
                {
                    twoCount = true;
                }
            }

            if (threeCount && twoCount)
            {
                return result = 25;
            }

            return result;                               
        }

        //Small straight 4/5 in consecutive order. return 30

        //Large straight 5/5 in consecutive order. return 40

        //yahtzeeValue 5/5 are equal return 50

        public int ChanceValue()
        {
            return this.Dice.Sum(x => x.CurrentValue);
        }

        //Helper method for checking if three or four kind.
        private int CheckKindValue(int kindValue)
        {
            var result = 0;

            foreach (var die in this.Dice)
            {

                var count = CountDice(die.CurrentValue);
                if (count == kindValue)
                {
                    return result = this.Dice.Sum(x => x.CurrentValue);
                }

            }

            return result;
        }
    }
}
